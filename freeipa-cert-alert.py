#!/usr/bin/env python3
# File: alert.py
# Location: https://gitlab.com/bgstack15/freeipa-cert-alert
# Author: bgstack15
# Startdate: 2021-10-27 14:00
# SPDX-License-Identifier: GPL-3.0
# Title: Script that Alerts For Expiring Certs
# Purpose: Send me alerts for certs that are about to expire
# History:
# Usage:
#    Set env: FREEIPA_SERVER FREEIPA_USERNAME FREEIPA_PASSWORD DAYS
# References:
#    https://python-freeipa.readthedocs.io/en/latest/
#    https://stackoverflow.com/questions/72899/how-do-i-sort-a-list-of-dictionaries-by-a-value-of-the-dictionary/73050#73050
#    https://stackoverflow.com/questions/6871016/adding-days-to-a-date-in-python/6871482#6871482
#    https://stackoverflow.com/questions/24027863/convert-a-utc-time-to-epoch
#    https://stackoverflow.com/questions/9989334/create-nice-column-output-in-python/9996049#9996049
# Improve:
# Dependencies:
#    Somehow this is not a requisite component of freeipa! Those are named python3-ipa*
#    fedora-req: python3-freeipa

import python_freeipa, json, datetime, os, sys
import dateutil.parser as dparser

# Functions
def show_list(inlist):
   col1max = 0
   col2max = 0
   col3max = 0
   for i in inlist:
      col1max = max(len(i['valid_not_before']),col1max)
      col2max = max(len(i['valid_not_after']),col2max)
      col3max = max(len(i['subject']),col3max)
   col1max = col1max+2
   col2max = col2max+2
   if len(inlist) > 0:
      a = "Not valid before"
      b = "Not valid after"
      c = "Subject"
      print(f"{a:<{col1max}} {b:<{col2max}} {c:<{col3max}}")
   for i in inlist:
      print(f"{i['valid_not_before']:<{col1max}} {i['valid_not_after']:<{col2max}} {i['subject']:<{col3max}}")

# Main
DAYS = os.getenv("DAYS",default=60)
try:
    DAYS = int(DAYS)
except:
    DAYS = 60

client = python_freeipa.ClientMeta(os.getenv("FREEIPA_SERVER"))
client.login(os.getenv("FREEIPA_USERNAME"),os.getenv("FREEIPA_PASSWORD"))

today = str(datetime.date.today( ))
future = str(datetime.date.today() + datetime.timedelta(days=DAYS))
results = client.cert_find(o_validnotafter_from=today,o_validnotafter_to=future)
certs = results['result']
# Sort
certs = sorted(certs,key=lambda d: int(dparser.parse(d['valid_not_after']).strftime('%s')))
if len(certs) > 0:
    print(f"Certificates expiring within {DAYS} days from {today}")
show_list(certs)
