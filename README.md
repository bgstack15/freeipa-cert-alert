# Readme for freeipa-cert-alert

## Overview
Freeipa-cert-alert is a small project that lists the certificates from an IPA server that will expire soon. The idea is to pass the output to a mail or logging utility.

## Using
You configure it with environment variables at runtime, including:

* `FREEIPA_SERVER`
* `FREEIPA_USERNAME`
* `FREEIPA_PASSWORD`
* `DAYS`

For some reason, domain name does not suffice as the server name. You must pick a server name. This is discoverable in a properly-functioning Kerberos domain with:

    dig +short -t srv _ldap._tcp.yourdomain.com | awk '{print $4}'

## Upstream
[This repository](https://gitlab.com/bgstack15/freeipa-cert-alert) is the original.

## Alternatives
Examine the output of `ipa cert-find` manually. Otherwise, I found no examples that do what I do here.

## Dependencies

### RPMs
* python3-freeipa

## References

## Differences from upstream
N/A
